const xlsx = require('json-as-xlsx');
const path = require('path');

exports.convertJsonList = (jsonList) => {

  let filePath = path.join(__dirname, '..', 'resources/' + jsonList.listTitle)
  let settings = {
    sheetName: "Lista",
    fileName: filePath,
    extraLength: 3
  }

  let titles = [
    {label: "Marchio", value: row => row.brand},
    {label: "Tipologia", value: row => row.typology},
    {label: "Categoria", value: row => row.category},
    {label: "Quantità", value: row => row.quantity},
    {label: "Prezzo", value: row => row.price},
    {label: "Totale", value: row => {
      if (row.price && row.quantity){
        return row.price * row.quantity;
      }else if(row.price && !row.quantity){
        return row.price;
      }else{
        return 0;
      }
      }},
    {label: "Codice", value: row => row.code},
    {label: "Genere", value: row => row.gender},
    {label: "Stagione", value: row => row.season},
    {label: "Box", value: row => {
        if(row.case != null)
          return row.case;
        else
          return 0;
      }
    }
  ]

  let contents = [];

  jsonList.rows.forEach( row => {
    let cleanFoo = {};
    for (var i in row) {
      if (row[i] !== null) {
        cleanFoo[i] = row[i];
      }
    }
    contents.push(cleanFoo);
  });

  try {
    xlsx( titles, contents, settings);// Will download the excel file
  }catch (e) {
    console.log(e);
  }
}


