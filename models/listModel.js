const mongoose = require("mongoose");

const listSchema = mongoose.Schema({
  listTitle: { type: String, required: true, unique: true},
  rows: [{
    brand: { type: String },
    category: { type: String},
    typology: { type: String},
    quantity: { type: Number},
    price: { type: Number},
    code: { type: String},
    gender: {type: String},
    season: { type: String},
    case: {type: Number},
  }],
  createdAt: { type: Date },
  updatedAt: { type: Date }
});

module.exports = mongoose.model("List", listSchema);
