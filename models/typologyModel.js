const mongoose = require("mongoose");

const typologySchema = mongoose.Schema({
  typologyName: { type: String, required: true },
});

module.exports = mongoose.model("Typology", typologySchema);
