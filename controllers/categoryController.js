const Category = require("../models/categoryModel");

/**
 * Add new categories
 */
exports.addNewCategory = (req, res, next) => {
  let newCategory = new Category({
    categoryName: req.body.categoryName
  });

  newCategory
    .save()
    .then((createdCategory, err) => {
      if (err) return next(err);
      res.status(201).json({
        message: "New Category Created"
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
      });
    });
};

/**
 * Delete category
 */
exports.deleteCategory = (req, res, next) => {
  console.log(req.params);
  let categoryToDelete = req.params.categoryId;
  Category.deleteOne({ _id: categoryToDelete }).then((result, err) => {
    if (err) return next(err);

    if (result.n > 0) {
      res.status(200).json({
        message: "Category deleted",
        details: result
      });
    } else {
      res.status(500).json({
        message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
      });
    }
  });
};

/**
 * Update category
 */
exports.updateCategory = (req, res, next) => {
  //TODO (Elimina e ricrea)
};

/**
 * Get all categories
 */
exports.getAllCategories = (req, res, next) => {
  const categories = Category.find().sort({ categoryName: 1 });

  categories
    .then((categoriesList, err) => {
      if (err) return next(err);

      res.status(200).json({
        message: "List of Categories",
        categories: categoriesList
      });
    })
    .catch(err => {
      res.status(500).json({
        message: "Impossibile recuperare l'elenco delle categorie, ti invitiamo a riprovare."
      });
    });
};
