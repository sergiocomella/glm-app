const User = require("../models/userModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.signupNewUser = (req, res, next) => {
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      const user = new User({
        email: req.body.email,
        password: hash
      });
      user.save()
        .then(result => {
          res.status(200).json({
            message: 'User created',
            result: result
          })
        })
        .catch(err => {
          res.status(500).json({
            error: {
              message: 'Le credenziali inserite non sono valide.'
            }
          })
        })
    });
};

exports.loginUser = (req, res, next) => {
  let fetchedUser;
  User.findOne({email: req.body.email})
    .then(user => {
      if(!!user){
        fetchedUser = user;
        return bcrypt.compare( req.body.password, user.password );
      }
    })
    .catch(err => {
      res.status(401).json({
        message: 'Email o Password errati. -- ' + err
      })
    })
    .then( result => {
      if (!result) {
        res.status(401).json({
          message: "Email o Password errati."
        });
      }else {
        const token = jwt.sign(
          {email: fetchedUser.email, userId: fetchedUser._id},
          process.env.JWT_KEY,
          {expiresIn: "1h"}
        );
        res.status( 200 ).json( {
          token: token,
          expiresIn: 3600,
          userId: fetchedUser._id,
          message: 'Authenticated'
        } )
      }
    })
    .catch(err =>{
       res.status(401).json({
        message: 'Email o Password errati. '
      })
    })
};
