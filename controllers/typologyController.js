const Typology = require( '../models/typologyModel' );

/**
 * Add new categories
 */
exports.addNewTypology = (req, res, next) => {
  let newTypology = new Typology( {
    typologyName: req.body.typologyName
  } );

  newTypology.save().then( (createdTypology, err) => {
    if (err) return next( err );
    res.status( 201 ).json( {
      message: "New Typology Created",
    } );
  } ).catch( error => {
    res.status( 500 ).json( {
      message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
    } )
  } )
};

/**
 * Delete Typology
 */
exports.deleteTypology = (req, res, next) => {
  let typologyToDelete = req.params.typologyId;
  Typology
    .deleteOne( {_id: typologyToDelete} )
    .then( (result, err) => {
      if (err) return next( err );

      if (result.n > 0) {
        res.status( 200 ).json( {
          message: "Typology deleted",
          details: result
        } );
      } else {
        res.status( 500 ).json( {
          message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
        });
      }
    } )
};

/**
 * Update Typology
 */
exports.updateTypology = (req, res, next) => {
//TODO (Elimina e ricrea)
};

/**
 * Get all categories
 */
exports.getAllTypologies = (req, res, next) => {
  const typologies = Typology.find();

  typologies.then( (typologiesList, err) => {
    if (err) return next( err );

    res.status( 200 ).json( {
      message: "List of Typologies",
      typologies: typologiesList
    } );
  } ).catch( err => {
    res.status( 500 ).json( {
      message: "Impossibile recuperare l'elenco delle tipologie, ti invitiamo a riprovare."
    } )
  } );
};
