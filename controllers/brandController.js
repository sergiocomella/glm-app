const Brand = require( '../models/brandModel' );

/**
 * Add new categories
 */
exports.addNewBrand = (req, res, next) => {
  let newBrand = new Brand( {
    brandName: req.body.brandName
  } );

  newBrand.save().then( (createdBrand, err) => {
    if (err) return next( err );
    res.status( 201 ).json( {
      message: "New Brand Created",
    } );
  } ).catch( error => {
    res.status( 500 ).json( {
      message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
    } )
  } )
};

/**
 * Delete Brand
 */
exports.deleteBrand = (req, res, next) => {
  let brandToDelete = req.params.brandId;
  Brand
    .deleteOne( {_id: brandToDelete} )
    .then( (result, err) => {
      if (err) return next( err );

      if (result.n > 0) {
        res.status( 200 ).json( {
          message: "Brand deleted",
          details: result
        } );
      } else {
        res.status( 500 ).json( {
          message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
        });
      }
    } )
};

/**
 * Update Brand
 */
exports.updateBrand = (req, res, next) => {
//TODO (Elimina e ricrea)
};

/**
 * Get all categories
 */
exports.getAllBrands = (req, res, next) => {
  const brands = Brand.find();

  brands.then( (brandsList, err) => {
    if (err) return next( err );

    res.status( 200 ).json( {
      message: "List of Brands",
      brands: brandsList
    } );
  } ).catch( err => {
    res.status( 500 ).json( {
      message: "Impossibile recuperare l'elenco dei marchi, ti invitiamo a riprovare."
    } )
  } );
};
