const List = require("../models/listModel");
const JsonXlsxConverter = require("../util/JsonXlsxConverter");
const fs = require("fs");
const path = require("path");

exports.createList = (req, res, next) => {
  let newList = new List({
    listTitle: req.body.listTitle,
    rows: [],
    createdAt: Date.now(),
    updatedAt: Date.now()
  });

  newList
    .save()
    .then((createdList, err) => {
      if (err) return next(err);

      res.status(200).json({
        message: "Lista creata con successo",
        id: createdList._id
      });
    })
    .catch(error => {
      res.status(500).json({
        message: "Impossibile creare la nuova lista, ti invitiamo a riprovare.",
        error: error
      });
    });
};

exports.getAllLists = (req, res, next) => {
  const lists = List.find()
    .select({ rows: 0 })
    .sort({ updatedAt: 1 });

  lists
    .then((lists, err) => {
      if (err) return next(err);

      res.status(200).json({
        message: "List of lists",
        lists: lists
      });
    })
    .catch(err => {
      res.status(500).json({
        message: "Impossibile recuperare tutte le liste, ti invitiamo a riprovare."
      });
    });
};

exports.getListById = (req, res, next) => {
  List.findOne({ _id: req.params.id })
    .then((list, err) => {
      if (err) return next(err);

      if (!!list) res.status(200).json(list);
      else res.status(404).json("List not found");
    })
    .catch(err => {
      res.status(500).json({
        message: "Impossibile recuperare la lista, ti invitiamo a riprovare."
      });
    });
};

exports.deleteList = (req, res, next) => {
  List.deleteOne({ _id: req.params.id }).then((result, err) => {
    if (err) return next(err);

    if (result.n > 0) {
      res.status(200).json({
        message: "List deleted",
        details: result
      });
    } else {
      res.status(500).json({
        message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
      });
    }
  });
};

//Add a row in one list by List ID
exports.addRow = (req, res, next) => {
  let listId = req.body.listId;
  let row = req.body.row;
  List.findByIdAndUpdate(
    { _id: listId },
    {
      $push: {
        rows: [row]
      },
      updatedAt: Date.now()
    }
  )
    .then((result, err) => {
      if (err) return next(err);

      if (!!result) res.status(200).json({ message: "Riga aggiunta" });
      else res.status(400).json({ message: "Lista non trovata" });
    })
    .catch(err => {
      res.status(500).json({
        message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
      });
      console.log(err);
    });
};

exports.getRowDetails = (req, res, next) => {
  List.findOne(
    { "rows._id": req.params.id })
    .then((doc, err) => {
      if (err) return next(err);

      if (!!doc) {
        res.status(200).json({
          message: "Row recuperata",
          row: doc.rows.id(req.params.id)

        });
      } else {
        res.status(500).json({
          message: "Impossibile recuperare i dettagli della riga, ti invitiamo a riprovare."
        });
      }
    });
}

exports.updateRowInList = (req, res, next) => {
  let listId = req.body.listId;
  let row = req.body.row;
  List.findByIdAndUpdate(
    { _id: listId },
    {
      $set: {
        "rows.$[row].brand": row.brand,
        "rows.$[row].category": row.category,
        "rows.$[row].typology": row.typology,
        "rows.$[row].code": row.code,
        "rows.$[row].gender": row.gender,
        "rows.$[row].price": row.price,
        "rows.$[row].quantity": row.quantity,
        "rows.$[row].season": row.season,
        "rows.$[row].case": row.case,
      }
    },
    {
      arrayFilters: [{ "row._id": row._id }],
      new: true,
      safe: true,
      upsert: true
    }
  )
    .then((result, err) => {
      if (err) return next(err);

      if (!!result)
        res.status(200).json({ message: "Riga Aggiornata"});
      else res.status(404).json({
        message: "Impossibile aggiornare la riga, ti invitiamo a riprovare."
      });
    })
    .catch(err => {
      res.status(500).json({
        message: "Impossibile inserire la riga aggiornata, ti invitiamo a riprovare."
      });
      console.log(err);
    });
};

exports.deleteRowInList = (req, res, next) => {

  List.findByIdAndUpdate(
    req.params.idList, { $pull: { "rows": { _id: req.params.idRow } } }, { safe: true, upsert: true },
    function(err, node) {
      if (err) { return res.status(500).json( {
        message: "Qualcosa è andato storto nell'eliminazione della riga."
      }); }
      return res.status(200).json({ message: "Row deleted"});
    });
}

exports.downloadList = (req, res, next) => {
  //let listId = req.params.id;
  let listId = req.body.listId;
  List.findOne({ _id: listId })
    .then((list, err) => {
      if (err) return next(err);

      if (!!list) {
        //Creo il file e lo conservo sul server
        JsonXlsxConverter.convertJsonList(list);
        //let filePath = 'resources/' + list.listTitle + '.xlsx';
        let filePath = path.join(
          __dirname,
          "..",
          "resources/" + list.listTitle + ".xlsx"
        );
        //Dopo aver fatto il downlaod cancello il file creato poco prima
        res.download(filePath, "", "", () => {
          try {
            fs.unlinkSync(filePath);
          } catch (err) {
            console.error(err);
          }
        });
      } else res.status(404).json( {
        message: "Impossibile recuperare la lista selezionata, ti invitiamo a riprovare."
      });
    })
    .catch(err => {
      res.status(500).json({
        message: "Impossibile completare l'operazione, ti invitiamo a riprovare."
      });
      console.log(err);
    });
};

