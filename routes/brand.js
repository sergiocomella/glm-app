let router = require("express").Router();
let controller = require("../controllers/brandController");
const checkAuth = require("../middleware/check-auth");

router
  .route("/add")
  .post(checkAuth, controller.addNewBrand);

router
  .route("/:brandId")
  .delete(checkAuth, controller.deleteBrand);

router
  .route("/all")
  .get(checkAuth, controller.getAllBrands);

module.exports = router;
