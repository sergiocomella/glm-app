let router = require("express").Router();
let controller = require("../controllers/typologyController");
const checkAuth = require("../middleware/check-auth");

router
  .route("/add")
  .post(checkAuth, controller.addNewTypology);

router
  .route("/:typologyId")
  .delete(checkAuth, controller.deleteTypology);

router
  .route("/all")
  .get(checkAuth, controller.getAllTypologies);

module.exports = router;
