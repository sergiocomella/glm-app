let router = require("express").Router();
let controller = require("../controllers/categoryController");
const checkAuth = require("../middleware/check-auth");

router
  .route("/add")
  .post(checkAuth, controller.addNewCategory);

router
  .route("/:categoryId")
  .delete(checkAuth, controller.deleteCategory);

router
  .route("/all")
  .get(checkAuth, controller.getAllCategories);

module.exports = router;
