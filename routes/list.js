let router = require("express").Router();
let listController = require("../controllers/listController");
const checkAuth = require("../middleware/check-auth");

router
  .route("/all")
  .get(checkAuth, listController.getAllLists)

router
  .route("/addList")
  .post(checkAuth, listController.createList);

router
  .route("/deleteList/:id")
  .delete(checkAuth, listController.deleteList);

router
  .route("/:id")
  .delete(checkAuth, listController.deleteList);

router
  .route("/list/:id")
  .get(checkAuth, listController.getListById);

router
  .route("/addRow")
  .put(checkAuth, listController.addRow);

router
  .route("/updateRow")
  .put(checkAuth, listController.updateRowInList);

router
  .route("/deleteRow/:idList/:idRow")
  .delete(checkAuth, listController.deleteRowInList);

router
  .route("/getRow/:id")
  .get(checkAuth, listController.getRowDetails);

router
  .route("/download")
  .post(checkAuth, listController.downloadList);

module.exports = router;
