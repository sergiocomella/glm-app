const createError = require("http-errors");
const express = require("express");
const cors = require('cors');
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");
const path = require("path");

const categoryRouter = require("./routes/category");
const brandRouter = require("./routes/brand");
const listRouter = require("./routes/list");
const typologyRouter = require("./routes/typology");
const userRouter = require("./routes/user");

const app = express();
//app.use(cors());
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

/** DATABASE **/
let uri =
  "mongodb+srv://sergio:" + process.env.MONGO_ATLAS_PW +
  "@cluster0.r2xvw.mongodb.net/" + process.env.MONGO_ATLAS_DBNAME +
    "?retryWrites=true&w=majority";

mongoose
  .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("Connesso al db");
  })
  .catch(() => {
    console.log("Connessione non riuscita al db");
  });

mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Create link to Angular build directory
let distDir = __dirname + "/dist/client";
let resourcesDir = __dirname + "/resources/";
app.use(express.static(distDir));
app.use(express.static(resourcesDir));

app.use("/api/categories", categoryRouter);
app.use("/api/brands", brandRouter);
app.use("/api/typologies", typologyRouter);
app.use("/api/lists", listRouter);
app.use("/api/user", userRouter);

app.use((req, res, next) => {
  res.sendFile(path.join(__dirname, 'dist', 'client', 'index.html'))
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log({ err });
});

module.exports = app;
